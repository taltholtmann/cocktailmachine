# Arduino based CocktailMachine

## Labeling of the circuit board
| label | purpose |
|---|---|
| A | arduino 12V power(Vin)  |
| A_  | arduino 5V ground  |
|  L | outer LEDs |
| AL | arduino outer LED signal |
| AB | front push buttons |
| 1-9 | filter 12V |
| 10 | pump 24V |
| M | amount Poti |
| LCD | LCD |
| AP | poti connection to arduinos |
| Li | inner led clock |

## Arduino Pins
| Pin | purpose |
|---|---|
| 2,3,4,5 | front push buttons |
| 52 | led clock signal |
| 53 | outer led signal |
| A0 | amounrt poti |
| 30-38 | filter 1-9 |
| 39 | pump |
| SDA / SDC | I2C LCD |