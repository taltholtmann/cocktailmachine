// Buttons
byte soleroButton = 5;
byte sexOnTheBeachButton = 6;
byte buttonNotInUse1 = 7;
byte buttonNotInUse2 = 8;
byte cleanButton = 9;

// ventile
byte vodkaVentil = 32;
byte orangeJuiceVentil = 34;
byte cranberryVentil = 35;
byte airVentil = 33;

//pump
byte PUMP = 30;

//in cl
float vodkaPerSecond = 0.9;
float orangePerSecond = 0.8;
float maracujaPerSecond = 0.9;
float cranberryPerSecond = 0.9;

// Debug output
byte ledPin = 13;

void setup() {
  Serial.begin(9600);
  // Pump
  pinMode(PUMP,OUTPUT);
  //ventile
  pinMode(vodkaVentil,OUTPUT);
  pinMode(orangeJuiceVentil,OUTPUT);
  pinMode(cranberryVentil,OUTPUT);
  pinMode(airVentil,OUTPUT);

  digitalWrite(PUMP,LOW);
  digitalWrite(vodkaVentil,LOW);
  digitalWrite(orangeJuiceVentil,LOW);
  digitalWrite(cranberryVentil,LOW);
  digitalWrite(airVentil,LOW);
  
  //Buttons
  pinMode(soleroButton,INPUT_PULLUP);
  pinMode(sexOnTheBeachButton,INPUT_PULLUP);
  pinMode(cleanButton,INPUT_PULLUP);
  pinMode(buttonNotInUse1,INPUT_PULLUP);
  pinMode(buttonNotInUse2,INPUT_PULLUP);

  //Serial.write("Call!");
  pinMode(ledPin,OUTPUT);
}

void loop() {
  
  if(digitalRead(buttonNotInUse1) == LOW && digitalRead(buttonNotInUse2) == LOW) {
    clean();
  }
  
  if(digitalRead(soleroButton) == LOW) {
    mixSexOnTheBeachWithoutAlc(1);
    pumpAir();
  }

  if(digitalRead(sexOnTheBeachButton) == LOW) {
    mixSexOnTheBeach(1);
    pumpAir();
  }
  delay(500);
  
}

void mixSexOnTheBeach(float multiplier)
{
  orangeJuice(4 * multiplier);
  vodka(1.2 * multiplier);
  cranberry(4 * multiplier);
}

void mixSexOnTheBeachWithoutAlc(float multiplier) {
  orangeJuice(4 * multiplier);
  cranberry(4 * multiplier);  
}

void mixSoleroDrink(float multiplier)
{
  
  //maracuja(5 * multiplier);
  //orangeJuice(3 * multiplier);
  //vodka(1.5 * multiplier);
}

void cranberry(float cl)
{
  float millisecounds = cl/cranberryPerSecond;
  pump(cranberryVentil,millisecounds * 1000);
}

void orangeJuice(float cl)
{
  float millisecounds = cl/orangePerSecond;
  pump(orangeJuiceVentil,millisecounds * 1000);
}
void vodka(float cl) 
{
  float millisecounds = cl/vodkaPerSecond;
  pump(vodkaVentil,millisecounds * 1000);
}

void maracuja(float cl)
{
//  float millisecounds = cl/maracujaPerSecond;
//  pump(maracujaVentil,millisecounds * 1000);
}

void pump(int ventil, float millisecounds)
{
  digitalWrite(ventil,HIGH);
  delay(900);
  digitalWrite(PUMP,HIGH);
  delay(millisecounds);
  digitalWrite(PUMP,LOW);
  digitalWrite(ventil,LOW);
}

void pumpAir() {
  digitalWrite(airVentil,HIGH);
  digitalWrite(PUMP,HIGH);
  delay(2000);
  digitalWrite(PUMP,LOW);
  digitalWrite(airVentil,LOW);
}

void clean() {
  pump(vodkaVentil, 10000);
  pump(orangeJuiceVentil, 10000);
  pump(cranberryVentil, 10000);
  pumpAir();
}

