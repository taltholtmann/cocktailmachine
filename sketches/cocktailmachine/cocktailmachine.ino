#include <Adafruit_NeoPixel.h>
#include <LiquidCrystal_I2C.h>

// Structs
struct RGB {
  char color[15];
  uint8_t r;
  uint8_t g;
  uint8_t b;
};

struct ANIMATIONCONFIG {
  uint32_t lastMillis;
  RGB colorOn;
  RGB colorOff;
  uint16_t delayMillis;
  uint8_t lastIndex;
  uint8_t modulo;
};

// pin configuration
const byte ledPin = 53;
const byte ledCount = 37;
const byte ledClockPin = 52;
const byte ledClockCount = 12;
const byte buttonPins[] = {2,3,4,5};
const byte filterPins[] = {30,31,32,33,34,35,36,37,38};
const byte pumpPin = 39;
const byte amountPoti = 0; // A0


// initialisation
Adafruit_NeoPixel ledClock = Adafruit_NeoPixel(ledClockCount, ledClockPin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel led = Adafruit_NeoPixel(ledCount, ledPin, NEO_GRB + NEO_KHZ800);
LiquidCrystal_I2C lcd(0x3F, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
uint16_t totalAmountInMl = 0;
byte lastLedClockColor = 4;


// config
#define DEBUG   //For debugging. To enable debugging uncomment this line
byte ledBrightness = 250;

// Fluids to Filter mapping
const byte filterAir = 0;
const byte filterAnanas = 1;
const byte filterOrangen = 2;
const byte filterWodka = 3;
const byte filterRum = 4;
const byte filterMaracuja = 5;
const byte filterEmpty6 = 6;
const byte filterEmpty7 = 7;
const byte filterEmpty8 = 8;

RGB colors[] = {
  {"Aus", 0, 0, 0},
  {"Weiss", 255, 255, 255},
  {"Rot", 255, 0, 0},
  {"Gruen", 0, 255, 0},
  {"Blau", 0, 0, 255},
  {"Pink", 253, 35, 253},
  {"Gelb", 248, 255, 27}
};

uint8_t ledClockPositions[] = {9,8,7,6,5,4,3,2,1,0,11,10};

ANIMATIONCONFIG outerLedAnimationConfig = {0, colors[4], colors[0], 80, 0, 10};
ANIMATIONCONFIG clockLedAnimationConfig = {0, colors[4], colors[0], 200, 0, 2};


// JUST FOR DEBUGGING //
#ifdef DEBUG    //Macros are usually in all capital letters.
#define DPRINT(...)    Serial.print(__VA_ARGS__)     //DPRINT is a macro, debug print
#define DPRINTLN(...)  Serial.println(__VA_ARGS__)   //DPRINTLN is a macro, debug print with new line
#else
#define DPRINT(...)     //now defines a blank line
#define DPRINTLN(...)   //now defines a blank line
#endif
// --- //

void updateLcd(void)
{
  int temp = readAmountPoti();
  temp = temp - totalAmountInMl;
  temp = abs(temp);
  
  if(temp > 5) {
    totalAmountInMl = readAmountPoti();
    lcd.setCursor(0, 1);
    lcd.print("        ");
    lcd.setCursor(0, 1);
    lcd.print(totalAmountInMl);
  }
}

void ledSetAll(Adafruit_NeoPixel &led, RGB rgb) {
  //DPRINT("ledSetAll: Setting all leds to: ");
  //DPRINTLN(rgb.color);
  for (uint8_t i = 0; i < led.numPixels(); i++) {
    //pixels.setPixelColor(i, pixels.Color(r, g, b));
    led.setPixelColor(i, rgb.r, rgb.g, rgb.b); // ?????
  }
  led.show();
}

void ledAnimation(Adafruit_NeoPixel &led, ANIMATIONCONFIG &a)
{
  uint32_t actualMillis = millis();
  if(a.lastMillis + a.delayMillis < actualMillis) {
    //DPRINTLN("ledAnimation: Next Animation iteration: ");
    //DPRINT("Last Index: ");
    //DPRINTLN(a.lastIndex);
    for (uint8_t i = 0; i < led.numPixels(); i++) {
      if((i+a.lastIndex) % a.modulo == 0) {
        led.setPixelColor(i, a.colorOn.r, a.colorOn.g, a.colorOn.b);
      }
      else {
        led.setPixelColor(i, a.colorOff.r, a.colorOff.g, a.colorOff.b);     
      }
    }
    led.show();
    
    if(a.lastIndex == a.modulo-1) {
      a.lastIndex = 0;
    }
    else {
      a.lastIndex ++;
    }
    a.lastMillis = actualMillis;
  }
}

void ledSetPixel(Adafruit_NeoPixel &led, byte number, RGB rgb)
{
  //DPRINT("ledSetPixel: Setting led ");
  //DPRINT(number); DPRINT(" to: ");
  //DPRINTLN(rgb.color);
  led.setPixelColor(number, rgb.r, rgb.g, rgb.b);
  led.show();
}

void ledClockAnimation(int duration)
{
  byte color = 0;
  if(lastLedClockColor == 4) {
    color = 6;
    lastLedClockColor = 6;
  } else {
    color = 4;
    lastLedClockColor = 4;
  }
  uint16_t waitTimeStep = duration / ledClockCount;
  for (uint8_t i = 0; i < ledClockCount; i++) {
    delay(waitTimeStep);
    ledSetPixel(ledClock, ledClockPositions[i], colors[color]);        
  }  
}

uint16_t readAmountPoti(void) 
{
  uint16_t amountPotiValue = analogRead(amountPoti);
  //DPRINT("amountPotiValue = ");
  //DPRINTLN(amountPotiValue);

  float amount = 200 + (amountPotiValue * 0.390625);
  
  //DPRINT("calculated ml = ");
  //DPRINTLN(amount);
  
  return (int) amount;
}

int calcDelay(float percent)
{
  int ml = (int)(totalAmountInMl / 100 * percent);
  DPRINT("calculated ml = ");
  DPRINTLN(ml);
  uint16_t temp = (int)(ml * 130.434);  
  DPRINT("calculated delay for the ml in sec: ");
  DPRINTLN(temp);
  return temp;
}

void pump(byte filter, float percent) 
{
  digitalWrite(filterPins[filter], LOW);
  digitalWrite(pumpPin, LOW);
  ledClockAnimation(calcDelay(percent));
  digitalWrite(filterPins[filter], HIGH);
  digitalWrite(pumpPin, HIGH);  
}

void finish(void)
{
  // First Pump some air
  digitalWrite(filterPins[0], LOW);
  digitalWrite(pumpPin, LOW);
  ledClockAnimation(3000);
  digitalWrite(filterPins[0], HIGH);
  digitalWrite(pumpPin, HIGH); 

  // Blink green to show its ready
  ledSetAll(ledClock, colors[0]);
  for (uint8_t i = 0; i < 3; i++) {
    ledSetAll(ledClock, colors[3]);
    delay(200);
    ledSetAll(ledClock, colors[0]);
    delay(200);
  }
}

void start(void)
{
  ledSetAll(ledClock, colors[0]);
  for (uint8_t i = 0; i < 3; i++) {
    ledSetAll(ledClock, colors[2]);
    delay(200);
    ledSetAll(ledClock, colors[0]);
    delay(200);
  }
}

// Cocktail functions //
// all amounts are percentages
void mixSimplyRed(void)
{
  start();
  pump(filterWodka, 22.73);
  pump(filterOrangen, 45.46);
  pump(filterMaracuja, 31.82);
  finish();    
}

void mixSimplyRed_free(void)
{
  start();
  pump(filterOrangen, 58.82);
  pump(filterMaracuja, 41.18);
  finish();    
}

void mixHurricane(void)
{
  start();
  pump(filterRum, 28.57);
  pump(filterAnanas, 23.81);
  pump(filterOrangen, 23.81);
  pump(filterMaracuja, 23.81);
  
  finish();    
}

void mixHurricane_free(void)
{
  start();
  pump(filterAnanas, 33.33);
  pump(filterOrangen, 33.33);
  pump(filterMaracuja, 33.33);
  finish();    
}

// END Cocktail functions //

bool isCleaningModeActive(void) 
{
  // to have enough time to press all 4 buttons
  delay(300);
  if(digitalRead(buttonPins[0]) == LOW && digitalRead(buttonPins[1]) == LOW && digitalRead(buttonPins[2]) == LOW && digitalRead(buttonPins[3]) == LOW){
    DPRINTLN("Cleaning");
    for (int i = 0; i <= 8; i++) {
      pump(i, 10);
    }
    finish();
    return true;
  }
  return false;
}

void setup() {
  
  #ifdef DEBUG
    Serial.begin(9600);
  #endif

  // initialize the outer bottle leds
  led.begin();
  led.setBrightness(ledBrightness);
  ledSetAll(led, colors[0]);

  // initialize the led clock
  ledClock.begin();
  ledClock.setBrightness(ledBrightness);
  ledSetAll(ledClock, colors[0]);
  

  DPRINTLN("initialize buttons");
  for(int i=0; i < sizeof(buttonPins); i++){
    pinMode(buttonPins[i], INPUT_PULLUP);
  }

  DPRINTLN("initialize filter");
  for(int i=0; i < sizeof(filterPins); i++){
    pinMode(filterPins[i], OUTPUT);
    digitalWrite(filterPins[i], HIGH);
  }  
  
  DPRINTLN("initialize pump");
  pinMode(pumpPin, OUTPUT);
  digitalWrite(pumpPin, HIGH);

  // Getting the bottle amount
  totalAmountInMl = readAmountPoti();

  DPRINTLN("initialize LCD");
  lcd.init();
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Amount in ml:");

  lcd.setCursor(0, 1);
  lcd.print(totalAmountInMl);  
}

void loop() {
  ledAnimation(led, outerLedAnimationConfig);
  ledAnimation(ledClock, clockLedAnimationConfig);
  updateLcd();

  if(digitalRead(buttonPins[0]) == LOW){
    if(!isCleaningModeActive) {
      DPRINTLN("Serving Simply Red");
      mixSimplyRed(); 
    }          
  }
  else if(digitalRead(buttonPins[1]) == LOW){
    if(!isCleaningModeActive) {
      DPRINTLN("Serving Simply Red free");
      mixSimplyRed_free(); 
    }      
  }
  else if(digitalRead(buttonPins[2]) == LOW){
    if(!isCleaningModeActive) {
      DPRINTLN("Serving Hurricane");
      mixHurricane();
    }           
  }
  else if(digitalRead(buttonPins[3]) == LOW){
    if(!isCleaningModeActive) {
      DPRINTLN("Serving Hurricane free");
      mixHurricane_free();  
    }         
  }
}
